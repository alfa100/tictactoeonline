#ifndef _TIC_TAC_TOE_

#define _TIC_TAC_TOE_
#include <vector>
#include <string>

using namespace std;
class TicTacToe
{
public:

	TicTacToe();
	~TicTacToe();

	//Starting Or Restart the Game Functions

	//Starting The Game
	void StartGame();
	//Restart The Game
	void RestartGame();

private:

	//Playing the Game Functions 

	//Printing The Board
	void PrintBoard();
	//Doing a turn
	void PlayTurn();
	//Checking if someone won
	void CheckWinner();
	//Changing the player turn
	void ChangePlayer();

	//Variables

	//the table of the Game
	vector<string> Table;
	//Current player
	bool Player;
	//If someone won
	bool Winner;
	//How many turns passed
	int Turns;
	//The symbol of the player
	string Sym;
};

#endif // ! _TIC_TAC_TOE_