#include <vector>
#include <string>
#include <iostream>
#include "TicTacToe.h"
#include <algorithm>
using namespace std;

TicTacToe::TicTacToe()
{

	//Create Basic Table of the game
	for (size_t i = 1; i < 10; i++) {
		this->Table.push_back(to_string(i));
	}

	//Initialize Variables
	this->Player = false;
	this->Winner = false;
	this->Turns = 0;
	this->Sym = "X";
}

TicTacToe::~TicTacToe()
{
}

//Starting The Game
void TicTacToe::StartGame()
{
	//If someone won or maxed out the table
	while(Turns != 9 && !this->Winner)
	{
		//Rotation of The game
		system("CLS");
		this->PrintBoard();
		this->PlayTurn();
		this->PrintBoard();
		this->CheckWinner();
		if (this->Winner)
		{
			cout << "The Winner is " << this->Sym << endl;
			continue;
		}
		this->ChangePlayer();
	}
	//Checking if its a tie
	if (!this->Winner)
	{
		cout << "It's a tie!"<<endl;
	}
}

//Restart The Game
void TicTacToe::RestartGame()
{
	//Setting up Basic game table
	this->Table.clear();
	for (size_t i = 1; i < 10; i++) {
		this->Table.push_back(to_string(i));
	}
	
	//Initialize Varilables 
	this->Player = false;
	this->Winner = false;
	this->Turns = 0;
	this->Sym = "X";
}

//Printing The Board
void TicTacToe::PrintBoard()
{
	vector<string>::const_iterator TableIterator;
	int i = 0;

	//Printing the whole table
	for (TableIterator = this->Table.begin(); TableIterator != this->Table.end(); TableIterator++,i++)
	{
		//Printing Down line every 3 numbers
		if (0 == i % 3)
		{
			cout << endl << "-------"<<endl<<"|";
		}
		cout << *TableIterator << "|";
	}

	cout << endl << "-------" << endl;
}

//Doing a turn
void TicTacToe::PlayTurn()
{
	int i = 0;	
	string PlayerInput = { 0 };

	//Input from the player
	cin >> PlayerInput;

	//Replacing the input with is symbol
	replace((this->Table).begin(), (this->Table).end(), PlayerInput, this->Sym);
}

//Checking if someone won
void TicTacToe::CheckWinner()
{

	//Checking rows
	for (size_t i = 0; i < 9; i+=3)
	{
		if (this->Table[i] != this->Sym)
		{
			continue;
		}
		if (this->Table[i+1] != this->Sym)
		{
			continue;
		}
		if (this->Table[i + 2] == this->Sym)
		{
			this->Winner= true;
		}
	}
	
	//Checking columns
	for (size_t i = 0; i < 3; i++)
	{
		if (this->Table[i] != this->Sym)
		{
			continue;
		}
		if (this->Table[i + 3] != this->Sym)
		{
			continue;
		}
		if (this->Table[i + 6] == this->Sym)
		{
			this->Winner = true;
		}
	}

	//Checking oblique
	for (size_t i = 0; i < 3; i += 2)
	{
		
		if (this->Table[i] != this->Sym)
		{
			continue;
		}
		if (this->Table[4] != this->Sym)
		{
			continue;
		}
		if (this->Table[8 - i ] == this->Sym)
		{
			this->Winner = true;
		}
	}

}

//Changing the player turn
void TicTacToe::ChangePlayer()
{
	//Changing the player stats and add to the turns
	this->Player = !this->Player;
	this->Turns++;

	if (this->Player) 
	{
		this->Sym = "O";
	}
	else 
	{
		this->Sym = "X";
	}
}