
#include <vector>
#include <string>
#include "TicTacToe.h"
#include <iostream>

using namespace std;

int main()
{
	TicTacToe * Game = new TicTacToe();
	string Res = { 0 };

	//Starting the first Game
	Game->StartGame();

	//Checking if the players want more games
	cout << "Do you want one more game? Y/N ";
	cin >> Res;

	//Case they want
	while (Res == "Y")
	{
		Game->RestartGame();
		Game->StartGame();
		cout << "Do you want one more game? Y/N ";
		cin >> Res;
	}

	//Cleaning the Game object
	Game->~TicTacToe();

	return 0;
	
}